// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  var error;
  for (var i = 0; i < entry.length; i++) {
    try {
      newRequire(entry[i]);
    } catch (e) {
      // Save first error but execute all entries
      if (!error) {
        error = e;
      }
    }
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  parcelRequire = newRequire;

  if (error) {
    // throw error from earlier, _after updating parcelRequire_
    throw error;
  }

  return newRequire;
})({"../../../../../usr/local/lib/node_modules/parcel-bundler/src/builtins/bundle-url.js":[function(require,module,exports) {
var bundleURL = null;

function getBundleURLCached() {
  if (!bundleURL) {
    bundleURL = getBundleURL();
  }

  return bundleURL;
}

function getBundleURL() {
  // Attempt to find the URL of the current script and use that as the base URL
  try {
    throw new Error();
  } catch (err) {
    var matches = ('' + err.stack).match(/(https?|file|ftp|chrome-extension|moz-extension):\/\/[^)\n]+/g);

    if (matches) {
      return getBaseURL(matches[0]);
    }
  }

  return '/';
}

function getBaseURL(url) {
  return ('' + url).replace(/^((?:https?|file|ftp|chrome-extension|moz-extension):\/\/.+)\/[^/]+$/, '$1') + '/';
}

exports.getBundleURL = getBundleURLCached;
exports.getBaseURL = getBaseURL;
},{}],"../../../../../usr/local/lib/node_modules/parcel-bundler/src/builtins/css-loader.js":[function(require,module,exports) {
var bundle = require('./bundle-url');

function updateLink(link) {
  var newLink = link.cloneNode();

  newLink.onload = function () {
    link.remove();
  };

  newLink.href = link.href.split('?')[0] + '?' + Date.now();
  link.parentNode.insertBefore(newLink, link.nextSibling);
}

var cssTimeout = null;

function reloadCSS() {
  if (cssTimeout) {
    return;
  }

  cssTimeout = setTimeout(function () {
    var links = document.querySelectorAll('link[rel="stylesheet"]');

    for (var i = 0; i < links.length; i++) {
      if (bundle.getBaseURL(links[i].href) === bundle.getBundleURL()) {
        updateLink(links[i]);
      }
    }

    cssTimeout = null;
  }, 50);
}

module.exports = reloadCSS;
},{"./bundle-url":"../../../../../usr/local/lib/node_modules/parcel-bundler/src/builtins/bundle-url.js"}],"node_modules/normalize.css/normalize.css":[function(require,module,exports) {

        var reloadCSS = require('_css_loader');
        module.hot.dispose(reloadCSS);
        module.hot.accept(reloadCSS);
      
},{"_css_loader":"../../../../../usr/local/lib/node_modules/parcel-bundler/src/builtins/css-loader.js"}],"src/styles/styles.styl":[function(require,module,exports) {
var reloadCSS = require('_css_loader');

module.hot.dispose(reloadCSS);
module.hot.accept(reloadCSS);
},{"./../assets/fonts/glacialindifference-regular-webfont.woff":[["glacialindifference-regular-webfont.a4f8ba9c.woff","src/assets/fonts/glacialindifference-regular-webfont.woff"],"src/assets/fonts/glacialindifference-regular-webfont.woff"],"./../assets/fonts/glacialindifference-bold-webfont.woff":[["glacialindifference-bold-webfont.99ccd0c3.woff","src/assets/fonts/glacialindifference-bold-webfont.woff"],"src/assets/fonts/glacialindifference-bold-webfont.woff"],"./../assets/fonts/glacialindifference-italic-webfont.woff":[["glacialindifference-italic-webfont.946e68b5.woff","src/assets/fonts/glacialindifference-italic-webfont.woff"],"src/assets/fonts/glacialindifference-italic-webfont.woff"],"_css_loader":"../../../../../usr/local/lib/node_modules/parcel-bundler/src/builtins/css-loader.js"}],"src/timer.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Timer =
/*#__PURE__*/
function () {
  function Timer(element) {
    _classCallCheck(this, Timer);

    this.element = element;
    this.seconds = 0;
    this.timer = setInterval(this.setTime.bind(this), 1000);
  }

  _createClass(Timer, [{
    key: "pad",
    value: function pad(val) {
      var valString = val + '';

      if (valString.length < 2) {
        return '0' + valString;
      } else {
        return valString;
      }
    }
  }, {
    key: "setTime",
    value: function setTime() {
      this.seconds++;
      this.element.innerHTML = "".concat(this.pad(parseInt(this.seconds / 3600, 10)), ":").concat(this.pad(parseInt(this.seconds / 60, 10)), ":").concat(this.pad(this.seconds % 60));
    }
  }, {
    key: "kill",
    value: function kill() {
      clearInterval(this.timer);
    }
  }]);

  return Timer;
}();

exports.default = Timer;
},{}],"src/player.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Player =
/*#__PURE__*/
function () {
  function Player(symbol) {
    _classCallCheck(this, Player);

    this.symbol = symbol;
    this.wins = 0;
    this.losses = 0;
    this.draws = 0;
  }

  _createClass(Player, [{
    key: "getWinsPercentage",
    value: function getWinsPercentage() {
      return this.wins || this.losses ? "".concat(Math.round(this.wins / (this.wins + this.losses) * 100), "%") : '0%';
    }
  }, {
    key: "getLossPercentage",
    value: function getLossPercentage() {
      return this.wins || this.losses ? "".concat(Math.round(this.losses / (this.wins + this.losses) * 100), "%") : '0%';
    }
  }]);

  return Player;
}();

exports.default = Player;
},{}],"src/assets/images/X_dark.svg":[function(require,module,exports) {
module.exports = "/X_dark.27aaeb7c.svg";
},{}],"src/assets/images/X_bright.svg":[function(require,module,exports) {
module.exports = "/X_bright.a02d4731.svg";
},{}],"src/assets/images/O_dark.svg":[function(require,module,exports) {
module.exports = "/O_dark.314a2888.svg";
},{}],"src/assets/images/O_bright.svg":[function(require,module,exports) {
module.exports = "/O_bright.704a9269.svg";
},{}],"src/tic-tac-toe.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _timer = _interopRequireDefault(require("./timer"));

var _player = _interopRequireDefault(require("./player"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var xDark = require('./assets/images/X_dark.svg');

var xBright = require('./assets/images/X_bright.svg');

var oDark = require('./assets/images/O_dark.svg');

var oBright = require('./assets/images/O_bright.svg');

var TicTacToe =
/*#__PURE__*/
function () {
  function TicTacToe(size) {
    _classCallCheck(this, TicTacToe);

    this.boardSize = size;
    this.init();
    new _timer.default(document.getElementById('total-time-label'));
  }

  _createClass(TicTacToe, [{
    key: "init",
    value: function init() {
      this.roundCount = 0;
      this.player1 = new _player.default('X');
      this.player2 = new _player.default('O');
      this.newRound();
    }
  }, {
    key: "renderBoard",
    value: function renderBoard() {
      var _this = this;

      var tableNode = document.getElementById('board-table');
      tableNode.setAttribute('class', "board board-".concat(this.boardSize));
      tableNode.innerHTML = '';
      this.board.forEach(function (row, rowIndex) {
        var tdNodes = row.map(function (cell, columnIndex) {
          var el = document.createElement('div');
          el.setAttribute('id', "board-cell-".concat(columnIndex, "-").concat(rowIndex));
          el.setAttribute('data-row', rowIndex);
          el.setAttribute('data-column', columnIndex);
          el.addEventListener('click', function (e) {
            this.makeMove(e);
          }.bind(_this));
          return el;
        });
        tdNodes.forEach(function (node) {
          return tableNode.appendChild(node);
        });
      });
    }
  }, {
    key: "makeMove",
    value: function makeMove(e) {
      var coordinates = e.target.dataset;

      if (!this.board[coordinates.row][coordinates.column]) {
        this.board[coordinates.row][coordinates.column] = this.currentPlayer.symbol;
        var moveElem = document.createElement('img');
        moveElem.setAttribute('id', "board-cell-".concat(coordinates.row, "-").concat(coordinates.column, "-asset"));
        moveElem.setAttribute('src', this.currentPlayer.symbol === 'X' ? xDark : oDark);
        e.target.appendChild(moveElem);
        this.checkForWin();
        this.currentPlayer = this.currentPlayer.symbol === this.player1.symbol ? this.player2 : this.player1;
      }
    }
  }, {
    key: "createMatrix",
    value: function createMatrix() {
      this.board = new Array(this.boardSize);

      for (var i = 0; i < this.boardSize; i++) {
        this.board[i] = new Array(this.boardSize).fill('');
      }
    }
  }, {
    key: "changeBoardSize",
    value: function changeBoardSize(size) {
      this.boardSize = size;
      this.newRound();
    }
  }, {
    key: "checkForWin",
    value: function checkForWin() {
      var _this2 = this;

      var winningRow = -1;
      var winningColumn = -1;
      var diagonal = false;

      var getColumn = function getColumn(colIndex) {
        return _this2.board.map(function (row) {
          return row[colIndex];
        });
      };

      var checkForWinningLine = function checkForWinningLine(line) {
        return line.every(function (play) {
          return play === _this2.currentPlayer.symbol;
        });
      };

      winningRow = this.board.findIndex(function (row) {
        return checkForWinningLine(row);
      });

      if (winningRow === -1) {
        for (var colIndex = 0; colIndex < this.boardSize; colIndex++) {
          var column = getColumn(colIndex);

          if (checkForWinningLine(column)) {
            winningColumn = colIndex;
            this.highlightWinningLine(false, winningColumn, false);
            this.updateStatistics(this.currentPlayer.symbol);
            break;
          }
        }
      } else {
        this.highlightWinningLine(winningRow, false, false);
        this.updateStatistics(this.currentPlayer.symbol);
      }

      if (winningRow === -1 && winningColumn === -1) {
        var _diagonal = this.board.map(function (row, i) {
          return row[i];
        });

        if (checkForWinningLine(_diagonal)) {
          this.highlightWinningLine(false, false, 'leading');
          this.updateStatistics(this.currentPlayer.symbol);
          _diagonal = true;
        } else {
          var reverseDiagonal = this.board.map(function (row, i) {
            return row[_this2.boardSize - (i + 1)];
          });

          if (checkForWinningLine(reverseDiagonal)) {
            this.highlightWinningLine(false, false, 'reverse');
            this.updateStatistics(this.currentPlayer.symbol);
            _diagonal = true;
          }
        }
      }

      if (!diagonal && winningColumn === -1 && winningRow === -1 && this.board.every(function (row) {
        return row.every(function (cell) {
          return cell !== '';
        });
      })) {
        this.updateStatistics(false);
      }
    }
  }, {
    key: "newRound",
    value: function newRound() {
      this.createMatrix();
      this.renderBoard();

      if (this.roundTimer) {
        this.roundTimer.kill();
      }

      this.currentPlayer = this.player1;
      this.roundTimer = new _timer.default(document.getElementById('round-timer'));
    }
  }, {
    key: "updatePlayersPercentages",
    value: function updatePlayersPercentages() {
      document.getElementById('player-1-win-percentage').innerHTML = this.player1.getWinsPercentage();
      document.getElementById('player-1-loss-percentage').innerHTML = this.player1.getLossPercentage();
      document.getElementById('player-2-win-percentage').innerHTML = this.player2.getWinsPercentage();
      document.getElementById('player-2-loss-percentage').innerHTML = this.player2.getLossPercentage();
    }
  }, {
    key: "updateStatistics",
    value: function updateStatistics(winner) {
      document.getElementsByClassName('played-matches__match-container')[this.roundCount].className += ' played-matches__match-container--filled';
      var matchHistory = document.getElementsByClassName('game-history__match-container')[this.roundCount];

      switch (winner) {
        case this.player1.symbol:
          matchHistory.innerHTML = 'P1';
          this.player1.wins++;
          this.player2.losses++;
          document.getElementById('player-1-win-count').innerHTML = this.player1.wins;
          this.updatePlayersPercentages();
          break;

        case this.player2.symbol:
          matchHistory.innerHTML = 'P2';
          this.player2.wins++;
          this.player1.losses++;
          document.getElementById('player-2-win-count').innerHTML = this.player2.wins;
          this.updatePlayersPercentages();
          break;

        default:
          matchHistory.innerHTML = '-';
          this.player1.draws++;
          this.player2.draws++;
          break;
      }

      if (this.player1.wins === 5 || this.player2.wins === 5) {
        this.showFinalResults(winner);
      } else if (this.roundCount === 8) {
        if (this.player1.wins !== this.player2.wins) {
          this.showFinalResults(winner);
        } else {
          this.showFinalResults(false);
        }
      } else {
        this.roundCount++;
        this.showRoundEndPopup(winner);
        this.currentPlayer = this.player1;
      }
    }
  }, {
    key: "highlightWinningLine",
    value: function highlightWinningLine(indexRow, indexColumn, diagonal) {
      if (indexRow !== false) {
        for (var i = 0; i < this.boardSize; i++) {
          document.getElementById("board-cell-".concat(indexRow, "-").concat(i, "-asset")).setAttribute('src', this.board[indexRow][i] === 'X' ? xBright : oBright);
        }
      } else if (indexColumn !== false) {
        for (var _i = 0; _i < this.boardSize; _i++) {
          document.getElementById("board-cell-".concat(_i, "-").concat(indexColumn, "-asset")).setAttribute('src', this.board[_i][indexColumn] === 'X' ? xBright : oBright);
        }
      } else if (diagonal === 'leading') {
        for (var _i2 = 0; _i2 < this.boardSize; _i2++) {
          document.getElementById("board-cell-".concat(_i2, "-").concat(_i2, "-asset")).setAttribute('src', this.board[_i2][_i2] === 'X' ? xBright : oBright);
        }
      } else if (diagonal === 'reverse') {
        for (var _i3 = 0; _i3 < this.boardSize; _i3++) {
          document.getElementById("board-cell-".concat(_i3, "-").concat(this.boardSize - (_i3 + 1), "-asset")).setAttribute('src', this.board[_i3][this.boardSize - (_i3 + 1)] === 'X' ? xBright : oBright);
        }
      }
    }
  }, {
    key: "showRoundEndPopup",
    value: function showRoundEndPopup(winner) {
      document.getElementById('popup').className = 'end-game-popup__container';
      document.getElementById('popup-label').innerHTML = winner ? "".concat(winner === 'X' ? 'Player 1' : 'Player 2', " won! \uD83E\uDD73") : "It's a \uD83D\uDC54! (get it?)";
      var button = document.getElementById('popup-action');
      button.innerHTML = 'Rematch!';
      button.addEventListener('click', this.closeAndRestart.bind(this));
    }
  }, {
    key: "showFinalResults",
    value: function showFinalResults(winner) {
      document.getElementById('popup').className = 'end-game-popup__container';
      document.getElementById('popup-label').innerHTML = winner ? "".concat(winner === 'X' ? 'Player 1' : 'Player 2', " has won this game! \uD83E\uDD42") : "Well, I guess no one wins \uD83E\uDD37\uD83C\uDFFB\u200D\u2642\uFE0F";
      var button = document.getElementById('popup-action');
      button.innerHTML = 'Rematch!';
      button.addEventListener('click', this.showStats.bind(this));
    }
  }, {
    key: "closeAndRestart",
    value: function closeAndRestart() {
      document.getElementById('popup').className = 'end-game-popup__container--hidden';
      this.newRound();
    }
  }, {
    key: "showStats",
    value: function showStats() {
      document.getElementById('popup').className = 'end-game-popup__container--hidden';
      document.getElementById('stats').scrollIntoView({
        behavior: 'smooth'
      });
      document.getElementById('reboot-game-button').className = document.getElementById('reboot-game-button').className.split(' ')[0];
    }
  }, {
    key: "rebootGame",
    value: function rebootGame() {
      for (var i = 0; i <= this.roundCount; i++) {
        document.getElementsByClassName('played-matches__match-container')[i].className = 'played-matches__match-container';
        document.getElementsByClassName('game-history__match-container')[i].innerHTML = '';
      }

      this.init();
      document.getElementById('player-1-win-count').innerHTML = 0;
      document.getElementById('player-2-win-count').innerHTML = 0;
      this.updatePlayersPercentages();
      document.getElementById('reboot-game-button').className += ' hidden';
      document.getElementById('game').scrollIntoView({
        behavior: 'smooth'
      });
    }
  }]);

  return TicTacToe;
}();

exports.default = TicTacToe;
},{"./timer":"src/timer.js","./player":"src/player.js","./assets/images/X_dark.svg":"src/assets/images/X_dark.svg","./assets/images/X_bright.svg":"src/assets/images/X_bright.svg","./assets/images/O_dark.svg":"src/assets/images/O_dark.svg","./assets/images/O_bright.svg":"src/assets/images/O_bright.svg"}],"src/index.js":[function(require,module,exports) {
"use strict";

require("normalize.css");

require("./styles/styles.styl");

var _ticTacToe = _interopRequireDefault(require("./tic-tac-toe"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var headerHeight = 80;
window.addEventListener('scroll', function () {
  document.getElementById('page-header').className = window.pageYOffset > headerHeight ? 'header' : 'header--transparent';
});
window.app = new _ticTacToe.default(3);
},{"normalize.css":"node_modules/normalize.css/normalize.css","./styles/styles.styl":"src/styles/styles.styl","./tic-tac-toe":"src/tic-tac-toe.js"}],"../../../../../usr/local/lib/node_modules/parcel-bundler/src/builtins/hmr-runtime.js":[function(require,module,exports) {
var global = arguments[3];
var OVERLAY_ID = '__parcel__error__overlay__';
var OldModule = module.bundle.Module;

function Module(moduleName) {
  OldModule.call(this, moduleName);
  this.hot = {
    data: module.bundle.hotData,
    _acceptCallbacks: [],
    _disposeCallbacks: [],
    accept: function (fn) {
      this._acceptCallbacks.push(fn || function () {});
    },
    dispose: function (fn) {
      this._disposeCallbacks.push(fn);
    }
  };
  module.bundle.hotData = null;
}

module.bundle.Module = Module;
var checkedAssets, assetsToAccept;
var parent = module.bundle.parent;

if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== 'undefined') {
  var hostname = "" || location.hostname;
  var protocol = location.protocol === 'https:' ? 'wss' : 'ws';
  var ws = new WebSocket(protocol + '://' + hostname + ':' + "54970" + '/');

  ws.onmessage = function (event) {
    checkedAssets = {};
    assetsToAccept = [];
    var data = JSON.parse(event.data);

    if (data.type === 'update') {
      var handled = false;
      data.assets.forEach(function (asset) {
        if (!asset.isNew) {
          var didAccept = hmrAcceptCheck(global.parcelRequire, asset.id);

          if (didAccept) {
            handled = true;
          }
        }
      }); // Enable HMR for CSS by default.

      handled = handled || data.assets.every(function (asset) {
        return asset.type === 'css' && asset.generated.js;
      });

      if (handled) {
        console.clear();
        data.assets.forEach(function (asset) {
          hmrApply(global.parcelRequire, asset);
        });
        assetsToAccept.forEach(function (v) {
          hmrAcceptRun(v[0], v[1]);
        });
      } else if (location.reload) {
        // `location` global exists in a web worker context but lacks `.reload()` function.
        location.reload();
      }
    }

    if (data.type === 'reload') {
      ws.close();

      ws.onclose = function () {
        location.reload();
      };
    }

    if (data.type === 'error-resolved') {
      console.log('[parcel] ✨ Error resolved');
      removeErrorOverlay();
    }

    if (data.type === 'error') {
      console.error('[parcel] 🚨  ' + data.error.message + '\n' + data.error.stack);
      removeErrorOverlay();
      var overlay = createErrorOverlay(data);
      document.body.appendChild(overlay);
    }
  };
}

function removeErrorOverlay() {
  var overlay = document.getElementById(OVERLAY_ID);

  if (overlay) {
    overlay.remove();
  }
}

function createErrorOverlay(data) {
  var overlay = document.createElement('div');
  overlay.id = OVERLAY_ID; // html encode message and stack trace

  var message = document.createElement('div');
  var stackTrace = document.createElement('pre');
  message.innerText = data.error.message;
  stackTrace.innerText = data.error.stack;
  overlay.innerHTML = '<div style="background: black; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; opacity: 0.85; font-family: Menlo, Consolas, monospace; z-index: 9999;">' + '<span style="background: red; padding: 2px 4px; border-radius: 2px;">ERROR</span>' + '<span style="top: 2px; margin-left: 5px; position: relative;">🚨</span>' + '<div style="font-size: 18px; font-weight: bold; margin-top: 20px;">' + message.innerHTML + '</div>' + '<pre>' + stackTrace.innerHTML + '</pre>' + '</div>';
  return overlay;
}

function getParents(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return [];
  }

  var parents = [];
  var k, d, dep;

  for (k in modules) {
    for (d in modules[k][1]) {
      dep = modules[k][1][d];

      if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) {
        parents.push(k);
      }
    }
  }

  if (bundle.parent) {
    parents = parents.concat(getParents(bundle.parent, id));
  }

  return parents;
}

function hmrApply(bundle, asset) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (modules[asset.id] || !bundle.parent) {
    var fn = new Function('require', 'module', 'exports', asset.generated.js);
    asset.isNew = !modules[asset.id];
    modules[asset.id] = [fn, asset.deps];
  } else if (bundle.parent) {
    hmrApply(bundle.parent, asset);
  }
}

function hmrAcceptCheck(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (!modules[id] && bundle.parent) {
    return hmrAcceptCheck(bundle.parent, id);
  }

  if (checkedAssets[id]) {
    return;
  }

  checkedAssets[id] = true;
  var cached = bundle.cache[id];
  assetsToAccept.push([bundle, id]);

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    return true;
  }

  return getParents(global.parcelRequire, id).some(function (id) {
    return hmrAcceptCheck(global.parcelRequire, id);
  });
}

function hmrAcceptRun(bundle, id) {
  var cached = bundle.cache[id];
  bundle.hotData = {};

  if (cached) {
    cached.hot.data = bundle.hotData;
  }

  if (cached && cached.hot && cached.hot._disposeCallbacks.length) {
    cached.hot._disposeCallbacks.forEach(function (cb) {
      cb(bundle.hotData);
    });
  }

  delete bundle.cache[id];
  bundle(id);
  cached = bundle.cache[id];

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    cached.hot._acceptCallbacks.forEach(function (cb) {
      cb();
    });

    return true;
  }
}
},{}]},{},["../../../../../usr/local/lib/node_modules/parcel-bundler/src/builtins/hmr-runtime.js","src/index.js"], null)
//# sourceMappingURL=/src.a2b27638.js.map