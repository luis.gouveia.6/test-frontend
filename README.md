# Front-End test

## Quick start

Hi there! I'm so glad to meet you!

So, for you to run this fantastic game you first haver to have parcel as a dependency in your computer. if you don't, just run this little command:

`npm install -g parcel-bundler`

Or, if you like to use yarn:

`yarn global add parcel-bundler`

And you are good to go! So now, all you need to do is run:

`npm | yarn start`

and the application should run instantly!

## About the game

This is the standard Tic tac Toe game we know and love, with the bonus that you can choose the grid size of the game, for some extra level stuff 😎.

I have ran out a little out of time, so if the styling structure feels a little out of place, well I acknowledge that, and I know I could do better. And also i could have made some animations that I love so much, but, in spite of this, I feel like the final product is very polished and I think it doesn't have 🐞, but well, we know these little ones like to sneak out once in a while.

I hope I hear from you guys very soon!

Luis Gouveia
