export default class Player {
    constructor(symbol) {
        this.symbol = symbol;
        this.wins = 0;
        this.losses = 0;
        this.draws = 0;
    }

    getWinsPercentage() {
        return this.wins || this.losses ? `${Math.round((this.wins/(this.wins + this.losses))*100)}%` : '0%';
    }
    getLossPercentage() {
        return this.wins || this.losses ? `${Math.round((this.losses/(this.wins + this.losses))*100)}%` : '0%';
    }
}