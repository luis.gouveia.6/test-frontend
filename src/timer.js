export default class Timer {
  constructor(element) {
    this.element = element;
    this.seconds = 0;
    this.timer = setInterval(this.setTime.bind(this), 1000);
  }

  pad(val) {
    var valString = val + '';
    if (valString.length < 2) {
      return '0' + valString;
    } else {
      return valString;
    }
  }

  setTime() {
    this.seconds++;
    this.element.innerHTML = `${this.pad(parseInt(this.seconds / 3600, 10))}:${this.pad(
      parseInt(this.seconds / 60, 10)
    )}:${this.pad(this.seconds % 60)}`;
  }

  kill() {
    clearInterval(this.timer);
  }
}
