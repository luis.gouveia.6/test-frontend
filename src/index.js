import 'normalize.css';

import './styles/styles.styl';
import TicTacToe from './tic-tac-toe';

const headerHeight = 80;

window.addEventListener('scroll', function () {
    document.getElementById('page-header').className = window.pageYOffset > headerHeight ? 'header' : 'header--transparent'
});

window.app = new TicTacToe(3);