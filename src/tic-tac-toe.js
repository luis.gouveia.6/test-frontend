import Timer from './timer';
import Player from './player';

const xDark = require('./assets/images/X_dark.svg');
const xBright = require('./assets/images/X_bright.svg');
const oDark = require('./assets/images/O_dark.svg');
const oBright = require('./assets/images/O_bright.svg');

export default class TicTacToe {
  constructor(size) {
    this.boardSize = size;
    this.init();
    new Timer(document.getElementById('total-time-label'));
  }

  init() {
    this.roundCount = 0;
    this.player1 = new Player('X');
    this.player2 = new Player('O');
    this.newRound();
  }

  renderBoard() {
    const tableNode = document.getElementById('board-table');
    tableNode.setAttribute('class', `board board-${this.boardSize}`);
    tableNode.innerHTML = '';
    this.board.forEach((row, rowIndex) => {
      const tdNodes = row.map((cell, columnIndex) => {
        const el = document.createElement('div');
        el.setAttribute('id', `board-cell-${columnIndex}-${rowIndex}`);
        el.setAttribute('data-row', rowIndex);
        el.setAttribute('data-column', columnIndex);
        el.addEventListener(
          'click',
          function (e) {
            this.makeMove(e);
          }.bind(this)
        );
        return el;
      });
      tdNodes.forEach(node => tableNode.appendChild(node));
    });
  }

  makeMove(e) {
    const coordinates = e.target.dataset;

    if (!this.board[coordinates.row][coordinates.column]) {
      this.board[coordinates.row][coordinates.column] = this.currentPlayer.symbol;
      const moveElem = document.createElement('img');
      moveElem.setAttribute('id', `board-cell-${coordinates.row}-${coordinates.column}-asset`);
      moveElem.setAttribute('src', this.currentPlayer.symbol === 'X' ? xDark : oDark);
      e.target.appendChild(moveElem);
      this.checkForWin();
      this.currentPlayer = this.currentPlayer.symbol === this.player1.symbol ? this.player2 : this.player1;
    }
  }

  createMatrix() {
    this.board = new Array(this.boardSize);
    for (let i = 0; i < this.boardSize; i++) {
      this.board[i] = new Array(this.boardSize).fill('');
    }
  }

  changeBoardSize(size) {
    this.boardSize = size;
    this.newRound();
  }

  checkForWin() {
    let winningRow = -1;
    let winningColumn = -1;
    let diagonal = false;

    const getColumn = colIndex => this.board.map(row => row[colIndex]);
    const checkForWinningLine = line => line.every(play => play === this.currentPlayer.symbol);

    winningRow = this.board.findIndex(row => checkForWinningLine(row));

    if (winningRow === -1) {
      for (let colIndex = 0; colIndex < this.boardSize; colIndex++) {
        let column = getColumn(colIndex);

        if (checkForWinningLine(column)) {
          winningColumn = colIndex;
          this.highlightWinningLine(false, winningColumn, false);
          this.updateStatistics(this.currentPlayer.symbol);
          break;
        }
      }
    } else {
      this.highlightWinningLine(winningRow, false, false);
      this.updateStatistics(this.currentPlayer.symbol);
    }
    if (winningRow === -1 && winningColumn === -1) {
      let diagonal = this.board.map((row, i) => row[i]);
      if (checkForWinningLine(diagonal)) {
        this.highlightWinningLine(false, false, 'leading');
        this.updateStatistics(this.currentPlayer.symbol);
        diagonal = true;
      } else {
        let reverseDiagonal = this.board.map((row, i) => row[this.boardSize - (i + 1)]);
        if (checkForWinningLine(reverseDiagonal)) {
          this.highlightWinningLine(false, false, 'reverse');
          this.updateStatistics(this.currentPlayer.symbol);
          diagonal = true;
        }
      }
    }

    if (
      !diagonal &&
      winningColumn === -1 &&
      winningRow === -1 &&
      this.board.every(row => row.every(cell => cell !== ''))
    ) {
      this.updateStatistics(false);
    }
  }

  newRound() {
    this.createMatrix();
    this.renderBoard();
    if (this.roundTimer) {
      this.roundTimer.kill();
    }
    this.currentPlayer = this.player1;
    this.roundTimer = new Timer(document.getElementById('round-timer'));
  }

  updatePlayersPercentages() {
    document.getElementById('player-1-win-percentage').innerHTML = this.player1.getWinsPercentage();
    document.getElementById('player-1-loss-percentage').innerHTML = this.player1.getLossPercentage();
    document.getElementById('player-2-win-percentage').innerHTML = this.player2.getWinsPercentage();
    document.getElementById('player-2-loss-percentage').innerHTML = this.player2.getLossPercentage();
  }

  updateStatistics(winner) {
    document.getElementsByClassName('played-matches__match-container')[this.roundCount].className +=
      ' played-matches__match-container--filled';
    const matchHistory = document.getElementsByClassName('game-history__match-container')[this.roundCount];

    switch (winner) {
      case this.player1.symbol:
        matchHistory.innerHTML = 'P1';
        this.player1.wins++;
        this.player2.losses++;
        document.getElementById('player-1-win-count').innerHTML = this.player1.wins;
        this.updatePlayersPercentages();
        break;
      case this.player2.symbol:
        matchHistory.innerHTML = 'P2';
        this.player2.wins++;
        this.player1.losses++;
        document.getElementById('player-2-win-count').innerHTML = this.player2.wins;
        this.updatePlayersPercentages();
        break;
      default:
        matchHistory.innerHTML = '-';
        this.player1.draws++;
        this.player2.draws++;
        break;
    }
    if (this.player1.wins === 5 || this.player2.wins === 5) {
      this.showFinalResults(winner);
    } else if (this.roundCount === 8) {
      if (this.player1.wins !== this.player2.wins) {
        this.showFinalResults(winner);
      } else {
        this.showFinalResults(false);
      }
    } else {
      this.roundCount++;
      this.showRoundEndPopup(winner);
      this.currentPlayer = this.player1;
    }
  }

  highlightWinningLine(indexRow, indexColumn, diagonal) {
    if (indexRow !== false) {
      for (let i = 0; i < this.boardSize; i++) {
        document
          .getElementById(`board-cell-${indexRow}-${i}-asset`)
          .setAttribute('src', this.board[indexRow][i] === 'X' ? xBright : oBright);
      }
    } else if (indexColumn !== false) {
      for (let i = 0; i < this.boardSize; i++) {
        document
          .getElementById(`board-cell-${i}-${indexColumn}-asset`)
          .setAttribute('src', this.board[i][indexColumn] === 'X' ? xBright : oBright);
      }
    } else if (diagonal === 'leading') {
      for (let i = 0; i < this.boardSize; i++) {
        document
          .getElementById(`board-cell-${i}-${i}-asset`)
          .setAttribute('src', this.board[i][i] === 'X' ? xBright : oBright);
      }
    } else if (diagonal === 'reverse') {
      for (let i = 0; i < this.boardSize; i++) {
        document
          .getElementById(`board-cell-${i}-${this.boardSize - (i + 1)}-asset`)
          .setAttribute('src', this.board[i][this.boardSize - (i + 1)] === 'X' ? xBright : oBright);
      }
    }
  }

  showRoundEndPopup(winner) {
    document.getElementById('popup').className = 'end-game-popup__container';
    document.getElementById('popup-label').innerHTML = winner ?
      `${winner === 'X' ? 'Player 1' : 'Player 2'} won! 🥳` :
      `It's a 👔! (get it?)`;
    const button = document.getElementById('popup-action');
    button.innerHTML = 'Rematch!';
    button.addEventListener('click', this.closeAndRestart.bind(this));
  }

  showFinalResults(winner) {
    document.getElementById('popup').className = 'end-game-popup__container';
    document.getElementById('popup-label').innerHTML = winner ?
      `${winner === 'X' ? 'Player 1' : 'Player 2'} has won this game! 🥂` :
      `Well, I guess no one wins 🤷🏻‍♂️`;
    const button = document.getElementById('popup-action');
    button.innerHTML = 'Rematch!';
    button.addEventListener('click', this.showStats.bind(this));
  }

  closeAndRestart() {
    document.getElementById('popup').className = 'end-game-popup__container--hidden';
    this.newRound();
  }

  showStats() {
    document.getElementById('popup').className = 'end-game-popup__container--hidden';
    document.getElementById('stats').scrollIntoView({
      behavior: 'smooth'
    });
    document.getElementById('reboot-game-button').className = document
      .getElementById('reboot-game-button')
      .className.split(' ')[0];
  }

  rebootGame() {
    for (let i = 0; i <= this.roundCount; i++) {
      document.getElementsByClassName('played-matches__match-container')[i].className =
        'played-matches__match-container';
      document.getElementsByClassName('game-history__match-container')[i].innerHTML = '';
    }
    this.init();
    document.getElementById('player-1-win-count').innerHTML = 0;
    document.getElementById('player-2-win-count').innerHTML = 0;
    this.updatePlayersPercentages();
    document.getElementById('reboot-game-button').className += ' hidden';
    document.getElementById('game').scrollIntoView({
      behavior: 'smooth'
    });
  }
}